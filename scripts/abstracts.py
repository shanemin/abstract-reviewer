import pandas as pd

def clear_terminal():
    print(chr(27) + "[2J")

def print_stats(it, size, values):
    print('Paper ' + str(it+3) + ' / ' + str(size+3), end='\n\n')
    print('Accepted:  ' + str(values[1]))
    print('Rejected:  ' + str(values[0]))
    print('Unsure:    ' + str(values[3]), end='\n\n')

def print_abstract(title, abstract):
    print(title, '\n\n', abstract, end='\n\n')

df = pd.read_csv('query.csv')
df['status'] = df['status'].astype('str')
options = ['a','r','u','q']
clear_terminal()
for iteration, abstract in enumerate(df['abstract']):
    if df.at[iteration, 'status'] in options:
        # skip what we've already voted on
        continue
    ans = ''
    print_stats(iteration, len(df), df['status'].value_counts())
    print_abstract(df.at[iteration, 'title'], abstract)
    while ans not in options:
        ans = input('Accept(a), Reject(r), Unsure(u), Quit(q): ')
    if ans == 'q':
        exit(0)
    df.at[iteration, 'status'] = ans
    clear_terminal()
    df.to_csv('query.csv', index=False)
exit(0)

