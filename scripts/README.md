This is a tool to use after running your query by using the tool provided by https://github.com/l8518/msc-literature-study.

It will display each abstract one-at-a-time, giving you the option to tag each with Accept(a), Reject(r), Unknown(u), Quit(q), which is added to a new column 'status'. This autosaves after making each selection, and quitting/restarting the script will continue where you left off.

You will need to change the name of the query CSV file in the script to the one you want to read.

You may need to manually add the 'status' column to your query CSV.

